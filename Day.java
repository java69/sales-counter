public class Day {

	private String[] day;
	private int saleSize;
	private Sale[] dailySale;

	public Day(){
	
		day = new String[7];
		day[0] = "Sunday";
		day[1] = "Monday";
		day[2] = "Tuesday";
		day[3] = "Wednesday";
		day[4] = "Thursday";
		day[5] = "Friday";
		day[6] = "Saturday";
		
		dailySale = new Sale[20];
		saleSize = 0;

	}

	public Day(int arraySize){
	
		dailySale = new Sale[arraySize];
		saleSize = 0;
	
	}

	public void addSale(Sale sale) {

		if (saleSize < dailySale.length) {
			dailySale[saleSize] = sale;	
			saleSize++;
		
		}
	}

	

	public String toString(int d){

		String whatDay = day[d] + " has sales of:\n";
		
		for (int i = 0; i < saleSize; i++){
			
			whatDay = whatDay + dailySale[i].toString() + "\n";
		
		}
		return whatDay;
	}
}
