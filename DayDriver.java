/*This project is to allow a business owner to 
*be able to add up and average a list of 20 sales
*for each given day of the week to be able to  
*track which days they do the most business.
* @author Scott Hasserd
* @version 1.0
*
*
*
*
* *********************************************/
import java.util.Scanner;

public class DayDriver{ 
	public static void main(String args[]) {
	
		int weekDay = 0;	
		do {
			Scanner myScan = new Scanner(System.in);
			System.out.println("Enter Day of week (0 for Sunday, 6 for Saturday, 9 to quit): ");	
			weekDay = myScan.nextInt();
			
		if (weekDay == 9) { System.out.println("Bye!");} else {
			Day dayOfWeek = new Day();
			
					float sale;
					int i = 0;
					float total = 0;
					float average = 0;	
				do {
					System.out.println("Enter Sale, 0.00 to quit: ");
					sale = myScan.nextFloat();
					dayOfWeek.addSale(new Sale(sale));
					i++;
					total = total + sale;
					average = total / (i-1);	
				} while (sale != 0.00 && i < 20);
			
			System.out.println(dayOfWeek.toString(weekDay));
			System.out.println("Total is: $"  + total);
			System.out.println("Average is: $"  + average);
		
		 	}

		} while (weekDay != 9); 
	}
}
