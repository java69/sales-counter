public class Sale{
	
	private float saleItem;

	public Sale(float f){

		saleItem = f;
	
		}

	public String toString(){
		
		return  "Sale of: $" + saleItem;
	
	}

}
